const container = document.querySelector(".container");
const select = document.getElementById("news");
select.addEventListener("change", getValue);

function getValue() {
  const value = select.value;
  fetchNews(value);
}

function fetchNews(value = "all") {
  //   console.log(value);
  const url = `https://inshorts.deta.dev/news?category=${value}`;
  fetch(url)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      //   console.log(data);
      return renderNews(data);
    })
    .catch((err) => {
      console.error(err);
    });
}

function renderNews(news) {
  container.innerHTML = "";
  for (let idx = 0; idx < news.data.length; idx++) {
    const singleNews = news.data[idx];
    // const card = `
    //         <div class="card">
    //             <div class="news-img">
    //             <img src=${singleNews.imageUrl} class="img" alt="news img" />
    //             </div>
    //             <div class="news-content">
    //             <h2 class="title">${singleNews.title}</h2>
    //             <div class="author">
    //                 <strong>short by</strong>
    //                 <p class="author-name">${singleNews.author}</p>
    //                 <small>${singleNews.date} ${singleNews.time}</small>
    //             </div>
    //             <p>${singleNews.content}</p>
    //             <small><a href=${singleNews.readMoreUrl}>read more...</a></small>
    //             </div>
    //         </div>
    //     `;
    // container.innerHTML += card;

    const card = document.createElement("div");
    card.classList.add("card");

    const newsImage = document.createElement("div");
    newsImage.classList.add("news-img");

    const img = document.createElement("img");
    img.src = `${singleNews.imageUrl}`;
    img.classList.add("img");
    img.alt = "news img";

    newsImage.appendChild(img);
    card.appendChild(newsImage);

    const newsContent = document.createElement("div");
    newsContent.classList.add("news-content");

    const title = document.createElement("h2");
    title.classList.add("title");
    title.innerHTML = `${singleNews.title}`;

    newsContent.appendChild(title);

    const author = document.createElement("div");
    author.classList.add("author");
    const strong = document.createElement("strong");
    strong.innerHTML = "short by";
    const authorName = document.createElement("p");
    authorName.classList.add("author-name");
    authorName.innerHTML = `${singleNews.author}`;
    const date = document.createElement("small");
    date.innerHTML = `${singleNews.date} ${singleNews.time}`;

    author.appendChild(strong);
    author.appendChild(authorName);
    author.appendChild(date);
    newsContent.appendChild(author);

    const content = document.createElement("p");
    content.innerHTML = `${singleNews.content}`;
    newsContent.appendChild(content);

    const readMore = document.createElement("small");
    const anchor = document.createElement("a");
    anchor.innerHTML = "read more...";
    anchor.href = `${singleNews.readMoreUrl}`;
    readMore.appendChild(anchor);
    newsContent.appendChild(readMore);

    card.appendChild(newsContent);
    container.appendChild(card);
  }
}

window.onload = fetchNews();
